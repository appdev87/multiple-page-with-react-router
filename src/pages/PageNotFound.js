import React from 'react';
import { NavLink } from 'react-router-dom';

const PageNotFound = () => {

  return (
    <div>
      <h1>ERROR 404</h1>
      <h2>The page you're looking for cannot be found</h2>
      <NavLink to="/quotes">Back Home</NavLink>
    </div>
  );
};

export default PageNotFound;
