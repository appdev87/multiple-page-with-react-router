import React, { Fragment, useEffect } from 'react';
import { useParams, Route, Link, useRouteMatch } from 'react-router-dom';

import Comments from '../components/comments/Comments';
import HighlightedQuote from '../components/quotes/HighlightedQuote';

import useHttp from '../hooks/use-http';
import { getSingleQuote } from '../lib/api';
import LoadingSpinner from '../components/UI/LoadingSpinner';


const QuoteDetail = () => {
    const params = useParams();
    const match = useRouteMatch();

    const { quoteID } = params;

    const { sendRequest, status, data: loadedQuotes, error } = useHttp(getSingleQuote, true);


    useEffect(() => {

      sendRequest(quoteID)
        .then(response => {console.log(response);});

    }, [sendRequest, quoteID]);

    const quote = loadedQuotes.find(quote => quote.id === params.quoteID);

    if (status === 'pending') {
      return (<div className="centered">
        <LoadingSpinner/>
      </div>);
    }

    if (error) {
      return <p className="centered">{error}</p>;

    }

    if (!loadedQuotes.text) {
      return <p>No quote found.</p>;
    }

    return (
      <Fragment>
        <HighlightedQuote text={quote.text} author={quote.author}/>
        <Route path={match.path} exact>
          <div className="centered">
            <Link className="btn--flat" to={`${match.url}/comments`}>Load Comments</Link>
          </div>
        </Route>
        <Route path={`${match.path}/comments`}>
          <Comments/>
        </Route>
      </Fragment>
    );
  }
;

export default QuoteDetail;
